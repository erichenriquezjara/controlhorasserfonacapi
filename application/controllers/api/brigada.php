<?php
   
require APPPATH . 'controllers/REST_Controller.php';
     
class Brigada extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = -1, $httpResponseFlag = true)
	{
        $sql = "SELECT * FROM brigada WHERE IFNULL(cancelacion, 'N') != 'S'";

        if( $id != -1 ){
            $sql .= " AND id_brigada = " . $id;
        }

        $data =  $this->db->query($sql)->result();

        // Cargamos el tipo de brigada
        foreach( $data as $i ){
            $sqlTipoBrigada = "SELECT id_catalogo, clave, clave1, clave2, valor, valor1, valor2 FROM catalogo WHERE nombre_catalogo = 'TIPO BRIGADA' AND id_catalogo = '".$i->tipo_brigada."' AND IFNULL(cancelacion, 'N') != 'S'";
            $i->tipo_brigada = $this->db->query($sqlTipoBrigada)->first_row();
        }

        // Cargamos el Jefe de la Brigada
        foreach( $data as $i ){
            $sqlJefeBrigada = "SELECT col.* FROM colaborador col INNER JOIN catalogo ca ON col.cargo = ca.id_catalogo WHERE ca.valor = 'JEFE BRIGADA' AND col.id_colaborador = " . $i->jefe_brigada;
            $i->jefe_brigada = $this->db->query($sqlJefeBrigada)->first_row();
        }

        if( $httpResponseFlag){
            $this->response($data, REST_Controller::HTTP_OK);
        }
        else{
            return $data;
        }
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $estado = 'OK';
        $mensaje = 'Proceso Exitoso.';
        $input = $this->post();

        // Acá se limpia el objeto para guardar el ID en vez del objeto completo
        $input['tipo_brigada'] = $input['tipo_brigada']['id_catalogo'];
        $input['jefe_brigada'] = $input['jefe_brigada']['id_colaborador'];

        $this->db->set($input);
        $this->db->insert('brigada',$input);
        
        $error = $this->db->error();
        if( !empty($error['message']) ){
            $estado = 'ERROR';
            $mensaje = 'Ha ocurrido un problema - Error N° '.$error['code'];
        }

        $objResponse['estado'] = $estado;
        $objResponse['mensaje'] = $mensaje;
        $objResponse['objeto'] = $this->brigadaPorNombre_get( $input['nombre_brigada'], false );

        $this->response([$objResponse], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put()
    {
        $estado = 'OK';
        $mensaje = 'Proceso Exitoso.';
        $input = $this->put();

        // Acá se limpia el objeto para guardar el ID en vez del objeto completo
        $input['tipo_brigada'] = $input['tipo_brigada']['id_catalogo'];
        $input['jefe_brigada'] = $input['jefe_brigada']['id_colaborador'];

        $this->db->set($input);
        $this->db->update('brigada', $input, array('id_brigada'=>$input['id_brigada']));

        $error = $this->db->error();
        if( !empty($error['message']) ){
            $estado = 'ERROR';
            $mensaje = 'Ha ocurrido un problema - Error N° '.$error['code'];
        }

        $objResponse['estado'] = $estado;
        $objResponse['mensaje'] = $mensaje;
        $objResponse['objeto'] = $this->index_get( $input['id_brigada'], false );

        $this->response([$objResponse], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {

    }
    
    public function brigadaPorNombre_get($nombreBrigada = '-1', $httpResponseFlag = true){
        $sql = "SELECT * FROM brigada WHERE IFNULL(cancelacion, 'N') != 'S'";

        if( $nombreBrigada != '-1' ){
            $sql .= " AND nombre_brigada = '" . urldecode($nombreBrigada) . "'";
        }

        $data =  $this->db->query($sql)->result();

        if( $httpResponseFlag ){
            $this->response($data, REST_Controller::HTTP_OK);
        }
        else{
            return $data;
        }        
    }

    public function brigadaPorJefe_get($idColaborador, $httpResponseFlag = true){
        $sql = "SELECT id_brigada FROM brigada WHERE IFNULL(cancelacion, 'N') != 'S' AND jefe_brigada = " . $idColaborador;
        $data =  $this->db->query($sql)->first_row();

        if( !empty($data) ){
            $response = $this->index_get($data->id_brigada, false);
        }
        else{
            return [];
        }
        
        if( $httpResponseFlag ){
            $this->response($response[0], REST_Controller::HTTP_OK);
        }
        else{
            return $response[0];
        }
    }

    public function brigadasPorRolColaborador_get($idRol, $idColaborador){
        // Administrador
        // El Administrador vé todas las brigadas
        if ( $idRol == 1) {
            $response = $this->index_get(-1, false);
            if ( !empty($response) ){
                $this->response($response, REST_Controller::HTTP_OK);
                return;
            }
        }
        // Supervisor
        // El Supervisor vé las brigadas que supervisa
        else if ( $idRol == 2 ) {
            $response = $this->brigadaPorSupervisor_get($idColaborador, false);
            if ( !empty($response) ){
                $this->response($response, REST_Controller::HTTP_OK);
                return;
            }
        }
        // Jefe Brigada
        // El Jefe de Brigada sólo vé la brigada de la cual es jefe
        else if ( $idRol == 13 ) {
            $response = $this->brigadaPorJefe_get($idColaborador, false);
            if ( !empty($response) ){
                $this->response($response, REST_Controller::HTTP_OK);
                return;
            }
        }
        // Este es por si llega otra wea, aquí se devuelve vacío.
        $this->response([], REST_Controller::HTTP_OK);
        return;
    }

    public function brigadaPorSupervisor_get($idColaborador, $httpResponseFlag = true){
        $sql = "SELECT b.* FROM brigada b INNER JOIN supervisor_brigada sb ON sb.id_brigada = b.id_brigada INNER JOIN colaborador c ON sb.id_colaborador = c.id_colaborador WHERE c.id_colaborador = " . $idColaborador . " AND IFNULL(b.cancelacion, 'N') != 'S'";
        $data =  $this->db->query($sql)->result();
        $this->response($data, REST_Controller::HTTP_OK);

        if( $httpResponseFlag ){
            $this->response($data, REST_Controller::HTTP_OK);
        }
        else{
            return $data;
        }        
    } 

    public function brigadaSinAsignar_get(){
        $sql = "SELECT b.* FROM brigada b WHERE b.id_brigada NOT IN (SELECT id_brigada FROM supervisor_brigada) AND IFNULL(b.cancelacion, 'N') != 'S' ";
        $data =  $this->db->query($sql)->result();
        $this->response($data, REST_Controller::HTTP_OK);
    }      

    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function integrantes_put()
    {
        $estado = 'OK';
        $mensaje = 'Proceso Exitoso.';
        $input = $this->put();

        $brigada = $input['brigada'];
        $colaboradores = $input['colaboradores'];      
        
        $sql = "DELETE FROM colaborador_brigada WHERE id_brigada = " . $brigada['id_brigada'];
        $data =  $this->db->query($sql);
        $error = $this->db->error();     
        
        if( !empty($error['message']) ){
            $estado = 'ERROR';
            $mensaje = 'Ha ocurrido un problema - Error N° '.$error['code'];
            $objResponse['estado'] = $estado;
            $objResponse['mensaje'] = $mensaje;
            $objResponse['objeto'] = null;    
            $this->response([$objResponse], REST_Controller::HTTP_OK);            
        }        

        foreach( $colaboradores as $i ){
            $sqlInsert = "INSERT INTO colaborador_brigada (id_colaborador, id_brigada) VALUES (". $i['id_colaborador'] .", ". $brigada['id_brigada'] .")";
            $data =  $this->db->query($sqlInsert);
            $error = $this->db->error();
            if( !empty($error['message']) ){
                $estado = 'ERROR';
                $mensaje = 'Ha ocurrido un problema - Error N° '.$error['code'];
                $objResponse['estado'] = $estado;
                $objResponse['mensaje'] = $mensaje;
                $objResponse['objeto'] = null;    
                $this->response([$objResponse], REST_Controller::HTTP_OK);            
            }            
        }

        $sqlSelect = "SELECT c.* 
                        FROM colaborador c INNER JOIN colaborador_brigada cb ON c.id_colaborador = cb.id_colaborador 
                       WHERE cb.id_brigada = " . $brigada['id_brigada'];

        $data =  $this->db->query($sqlSelect)->result();        

        $objResponse['estado'] = $estado;
        $objResponse['mensaje'] = $mensaje;
        $objResponse['objeto'] = $data;

        $this->response([$objResponse], REST_Controller::HTTP_OK);
    }    


    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function registrarBrigadasPorSupervisor_put()
    {
        $estado = 'OK';
        $mensaje = 'Proceso Exitoso.';
        $input = $this->put();

        $brigadas = $input['brigadas'];
        $colaborador = $input['colaborador'];
        
        $sql = "DELETE FROM supervisor_brigada WHERE id_colaborador = " . $colaborador['id_colaborador'];
        $data =  $this->db->query($sql);
        $error = $this->db->error();     
        
        if( !empty($error['message']) ){
            $estado = 'ERROR';
            $mensaje = 'Ha ocurrido un problema - Error N° '.$error['code'];
            $objResponse['estado'] = $estado;
            $objResponse['mensaje'] = $mensaje;
            $objResponse['objeto'] = null;    
            $this->response([$objResponse], REST_Controller::HTTP_OK);            
        }        

        // Por cada Brigada agregamos la fila a la tabla.
        foreach( $brigadas as $i ){
            $sqlInsert = "INSERT INTO supervisor_brigada (id_colaborador, id_brigada) values (".$colaborador['id_colaborador'].",".$i['id_brigada'].")";
            $data =  $this->db->query($sqlInsert);
            $error = $this->db->error();
            if( !empty($error['message']) ){
                $estado = 'ERROR';
                $mensaje = 'Ha ocurrido un problema - Error N° '.$error['code'];
                $objResponse['estado'] = $estado;
                $objResponse['mensaje'] = $mensaje;
                $objResponse['objeto'] = null;    
                $this->response([$objResponse], REST_Controller::HTTP_OK);            
            }            
        }

        $data =  $this->brigadaPorSupervisor_get($colaborador['id_colaborador'], false);

        $objResponse['estado'] = $estado;
        $objResponse['mensaje'] = $mensaje;
        $objResponse['objeto'] = $data;

        $this->response([$objResponse], REST_Controller::HTTP_OK);
    }   
    
}