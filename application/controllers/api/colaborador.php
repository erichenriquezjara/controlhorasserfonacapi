<?php
   
require APPPATH . 'controllers/REST_Controller.php';
     
class Colaborador extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = -1, $httpResponseFlag = true)
	{
        // Quiero evitar traer todos los campos si no es necesario.
        // $sql = "SELECT id_colaborador, rut, nombres, apellido_paterno, apellido_materno, cargo, es_usuario, contrasena, (SELECT id_brigada FROM brigada where jefe_brigada = id_colaborador) as jefe_de_brigada FROM colaborador WHERE IFNULL(cancelacion, 'N') != 'S'";
        $sql = "SELECT id_colaborador, rut, nombres, apellido_paterno, apellido_materno, cargo, es_usuario, contrasena FROM colaborador WHERE IFNULL(cancelacion, 'N') != 'S'";

        if( $id != -1 ){
            $sql .= " AND id_colaborador = " . $id;
        }

        $data =  $this->db->query($sql)->result();

        
        foreach( $data as $i ){
            $sqlCargos = "SELECT id_catalogo, valor FROM catalogo WHERE nombre_catalogo = 'CARGOS' AND id_catalogo = '".$i->cargo."' AND IFNULL(cancelacion, 'N') != 'S'";
            $i->cargo = $this->db->query($sqlCargos)->first_row();
        }

        if( $httpResponseFlag){
            $this->response($data, REST_Controller::HTTP_OK);
        }
        else{
            return $data;
        }
    }

	public function sinbrigada_get()
	{
        $sql = "SELECT id_colaborador, rut, nombres, apellido_paterno, apellido_materno, cargo, es_usuario, contrasena FROM colaborador c WHERE IFNULL(cancelacion, 'N') != 'S' AND id_colaborador NOT IN (SELECT id_colaborador FROM colaborador_brigada)";
        $data =  $this->db->query($sql)->result();

        $this->response($data, REST_Controller::HTTP_OK);
    } 

	public function porbrigada_get($id_brigada)
	{
        $sql = "SELECT id_colaborador, rut, nombres, apellido_paterno, apellido_materno, cargo, es_usuario, contrasena FROM colaborador c WHERE IFNULL(cancelacion, 'N') != 'S' AND id_colaborador IN (SELECT id_colaborador FROM colaborador_brigada WHERE id_brigada = " . $id_brigada . ")";
        $data =  $this->db->query($sql)->result();

        foreach( $data as $i ){
            $sqlCargos = "SELECT id_catalogo, valor FROM catalogo WHERE nombre_catalogo = 'CARGOS' AND id_catalogo = '".$i->cargo."' AND IFNULL(cancelacion, 'N') != 'S'";
            $i->cargo = $this->db->query($sqlCargos)->first_row();
        }
        $this->response($data, REST_Controller::HTTP_OK);
    }

    public function porRol_get($rol)
    {
        $sql = "SELECT c.* FROM colaborador c INNER JOIN colaborador_rol cr ON c.id_colaborador = cr.id_colaborador INNER JOIN rol r ON r.id_rol = cr.id_rol WHERE UPPER(r.nombre_rol) = UPPER('".$rol."') AND c.es_usuario = 1 AND IFNULL(c.cancelacion, 'N') != 'S'";
        $data =  $this->db->query($sql)->result();
        $this->response($data, REST_Controller::HTTP_OK);
    } 
    
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $estado = 'OK';
        $mensaje = 'Proceso Exitoso.';
        $input = $this->post();
        unset($input['role']);
        // Acá se limpia el objeto para guardar el ID en vez del objeto completo
        $input['cargo'] = $input['cargo']['id_catalogo'];

        $this->db->set($input);
        $this->db->insert('colaborador',$input);

        $id_inserted = $this->db->insert_id();
        
        $error = $this->db->error();
        if( !empty($error['message']) ){
            $estado = 'ERROR';
            $mensaje = 'Ha ocurrido un problema - Error N° '.$error['code'];
        }

        $objResponse['estado'] = $estado;
        $objResponse['mensaje'] = $mensaje;
        $objResponse['objeto'] = $this->index_get( $id_inserted, false );
        // $objResponse['objeto'] = $this->colaboradorPorRut_get( $input['rut'], false );

        $this->response([$objResponse], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put()
    {
        $estado = 'OK';
        $mensaje = 'Proceso Exitoso.';
        $input = $this->put();

        // Acá se limpia el objeto para guardar el ID en vez del objeto completo
        $input['cargo'] = $input['cargo']['id_catalogo'];

        $this->db->set($input);
        $this->db->update('colaborador', $input, array('id_colaborador'=>$input['id_colaborador']));

        $error = $this->db->error();
        if( !empty($error['message']) ){
            $estado = 'ERROR';
            $mensaje = 'Ha ocurrido un problema - Error N° '.$error['code'].$error['message'];
        }

        $objResponse['estado'] = $estado;
        $objResponse['mensaje'] = $mensaje;
        $objResponse['objeto'] = $this->index_get( $input['id_colaborador'], false );

        $this->response([$objResponse], REST_Controller::HTTP_OK);
    }
    
    public function colaboradorPorRut_get($rut = -1, $httpResponseFlag = true){
        $sql = "SELECT id_colaborador, rut, nombres, apellido_paterno, apellido_materno, cargo, es_usuario, contrasena, (SELECT id_brigada FROM brigada where jefe_brigada = id_colaborador) as jefe_de_brigada FROM colaborador WHERE IFNULL(cancelacion, 'N') != 'S'";

        if( $rut != -1 ){
            $sql .= " AND rut = '" . $rut . "'";
        }

        $data =  $this->db->query($sql)->result();

        if( $httpResponseFlag ){
            $this->response($data, REST_Controller::HTTP_OK);
        }
        else{
            return $data;
        }        
    }

    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function jefeBrigadas_get()
	{
        $sql = "SELECT * FROM colaborador col INNER JOIN catalogo ca ON col.cargo = ca.id_catalogo WHERE ca.valor = 'JEFE BRIGADA'";
        $data =  $this->db->query($sql)->result();
        $this->response($data, REST_Controller::HTTP_OK);
    }
    
    public function colaboradorSiniestro_get($id_siniestro){

        $sql = "SELECT c.* FROM colaborador_siniestro cs join colaborador c  on c.id_colaborador = cs.id_colaborador where cs.id_siniestro =".$id_siniestro;
        $data =  $this->db->query($sql)->result();
        $this->response($data, REST_Controller::HTTP_OK);
    }

    public function colaboradorBrigada_get($id_brigada){
        $sql = "select c.id_colaborador, c.rut, c.nombres, c.apellido_paterno, c.apellido_materno, c.cargo from colaborador c 
        join colaborador_brigada cb on cb.id_colaborador = c.id_colaborador
        where cb.id_brigada =".$id_brigada;
        $data =  $this->db->query($sql)->result();

        foreach( $data as $i ){
            $sqlCargos = "SELECT id_catalogo, valor FROM catalogo WHERE nombre_catalogo = 'CARGOS' AND id_catalogo = '".$i->cargo."' AND IFNULL(cancelacion, 'N') != 'S'";
            $i->cargo = $this->db->query($sqlCargos)->first_row();
        }
        $this->response($data, REST_Controller::HTTP_OK);
    }
}