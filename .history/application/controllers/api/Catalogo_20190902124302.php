<?php
   
require APPPATH . 'controllers/REST_Controller.php';
     
class Catalogo extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($nombre_catalogo = '', $id = -1, $httpResponseFlag = true)
	{
        $sql = "SELECT id_catalogo, valor, valor1 FROM catalogo WHERE IFNULL(cancelacion, 'N') != 'S' AND nombre_catalogo = '".urldecode($nombre_catalogo)."'";

        if( $id != -1 ){
            $sql .= " AND id_catalogo = " . $id;
        }

        $sql .= " ORDER BY orden ASC";        

        $data =  $this->db->query($sql)->result();

        if( $httpResponseFlag){
            $this->response($data, REST_Controller::HTTP_OK);
        }
        else{
            return $data;
        }
	}
}