<?php
   
require APPPATH . 'controllers\REST_Controller.php';
     
class Brigada extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->db->get_where("brigada", ['id_brigada' => $id])->row_array();
        }
        else{
            $data = $this->db->get("brigada")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $input = $this->post();

        $this->db->set($input);
        $this->db->insert('brigada',$input);
     
        $this->response(['brigada created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put()
    {
        var_dump($this->put());
        $input = $this->put();
        $this->db->update('brigada', $input, array('id_brigada'=>$input['id_brigada']));
     
        $this->response(['brigada updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('brigada', array('id_brigada'=>$id));
        $this->db->
       
        $this->response(['brigada deleted successfully.'], REST_Controller::HTTP_OK);
    }    	
}