<?php
   
require APPPATH . 'controllers\REST_Controller.php';
     
class Brigada extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = -1, $httpResponseFlag = true)
	{
        $sql = "SELECT * FROM brigada WHERE IFNULL(cancelacion, 'N') != 'S'";

        if( $id != -1 ){
            $sql .= " AND id_brigada = " . $id;
        }

        $data =  $this->db->query($sql)->result();

        if( $httpResponseFlag){
            $this->response($data, REST_Controller::HTTP_OK);
        }
        else{
            return $data;
        }
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $estado = 'OK';
        $mensaje = 'Proceso Exitoso.';
        $input = $this->post();

        $this->db->set($input);
        $this->db->insert('brigada',$input);
        
        $error = $this->db->error();
        if( !empty($error['message']) ){
            $estado = 'ERROR';
            $mensaje = 'Ha ocurrido un problema - Error N° '.$error['code'];
        }

        $objResponse['estado'] = $estado;
        $objResponse['mensaje'] = $mensaje;
        $objResponse['objeto'] = $this->brigadaPorNombre_get( $input['rut'], false );

        $this->response([$objResponse], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put()
    {
        $estado = 'OK';
        $mensaje = 'Proceso Exitoso.';
        $input = $this->put();
        $this->db->set($input);
        $this->db->update('brigada', $input, array('id_brigada'=>$input['id_brigada']));

        $error = $this->db->error();
        if( !empty($error['message']) ){
            $estado = 'ERROR';
            $mensaje = 'Ha ocurrido un problema - Error N° '.$error['code'];
        }

        $objResponse['estado'] = $estado;
        $objResponse['mensaje'] = $mensaje;
        $objResponse['objeto'] = $this->index_get( $input['id_brigada'], false );

        $this->response([$objResponse], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {

    }
    
    public function brigadaPorNombre_get($nombreBrigada = '-1', $httpResponseFlag = true){
        $sql = "SELECT * FROM brigada WHERE IFNULL(cancelacion, 'N') != 'S'";

        if( $nombreBrigada != '-1' ){
            $sql .= " AND nombre_brigada = " . $nombreBrigada;
        }

        $data =  $this->db->query($sql)->result();

        if( $httpResponseFlag ){
            $this->response($data, REST_Controller::HTTP_OK);
        }
        else{
            return $data;
        }        
    }
}