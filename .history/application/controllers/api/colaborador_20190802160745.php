<?php
   
require APPPATH . 'controllers\REST_Controller.php';
     
class Colaborador extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $sql = "SELECT * FROM colaborador WHERE IFNULL(cancelacion, 'N') != 'S' AND id_colaborador = " . $id;
        }
        else{
            $sql = "SELECT * FROM colaborador WHERE IFNULL(cancelacion, 'N') != 'S'";
        }

        $data =  $this->db->query($sql)->result();
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $input = $this->post();
        $this->db->set($input);
        $this->db->insert('colaborador',$input);     
        $this->response(['Colaborador created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put()
    {
        $input = $this->put();
        $this->db->set($input);
        $this->db->update('colaborador', $input, array('id_colaborador'=>$input['id_colaborador']));
        $this->response(['Colaborador updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {

    }    	
}