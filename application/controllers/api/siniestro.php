<?php
   
require APPPATH . 'controllers/REST_Controller.php';
     
class Siniestro extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = -1, $httpResponseFlag = true)
	{
        $data = $this->siniestrosPorBrigada_get($id, -1, -1, -1, false);

        if( $httpResponseFlag ){
            $this->response($data, REST_Controller::HTTP_OK);
        }
        else{
            return $data;
        }        
    }
    
	public function siniestrosPorBrigada_get($id = -1, $brigada = -1, $fechaDesde = -1, $fechaHasta = -1, $httpResponseFlag = true)
	{
        $sql = "SELECT * FROM siniestro WHERE IFNULL(cancelacion, 'N') != 'S'";

        if( $id != -1 ){
            $sql .= " AND id_siniestro = " . $id;
        }

        if( $brigada != -1 ){
            $sql .= " AND id_brigada = " . $brigada;
        }

        if( $fechaDesde != -1 && $fechaHasta != -1){
            $fechaDesde = base64_decode($fechaDesde);
            $fechaHasta = base64_decode($fechaHasta);
            $sql .= " AND fecha_despacho between str_to_date('".$fechaDesde."', '%d/%m/%Y') AND str_to_date('".$fechaHasta."', '%d/%m/%Y')";
        }

        $sql .= " order by id_siniestro";
        $data =  $this->db->query($sql)->result();

        // Cargamos la brigada
        foreach( $data as $i ){
            $sqlBrigada = "select * from brigada where id_brigada =".$i->id_brigada;
            $i->brigada = $this->db->query($sqlBrigada)->first_row();
        }

        // Cargamos el Lugar o Destino
        foreach( $data as $i ){
            $sqlLugarDestino = "SELECT id_catalogo, valor, valor1 FROM catalogo WHERE nombre_catalogo = 'LUGAR_DESTINO' AND id_catalogo = '".$i->lugar_destino."' AND IFNULL(cancelacion, 'N') != 'S'";
            $i->lugar_destino_cat = $this->db->query($sqlLugarDestino)->first_row();
        }        

        if( $httpResponseFlag ){
            $this->response($data, REST_Controller::HTTP_OK);
        }
        else{
            return $data;
        }
	}    
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $estado = 'OK';
        $mensaje = 'Proceso Exitoso.';
        $input = $this->post();
        $this->db->set($input);
        $this->db->insert('siniestro',$input);
        $id_inserted = $this->db->insert_id();      
        
        $error = $this->db->error();
        if( !empty($error['message']) ){
            $estado = 'ERROR';
            $mensaje = 'Ha ocurrido un problema - Error N° '.$error['code'];
        }

        $objResponse['estado'] = $estado;
        $objResponse['mensaje'] = $mensaje;
        $objResponse['objeto'] = $this->index_get($id_inserted, false );

        $this->response([$objResponse], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put()
    {
        $estado = 'OK';
        $mensaje = 'Proceso Exitoso.';
        $input = $this->put();

        $colaboradores = $input['colaboradores'];
        unset($input['colaboradores']);
        unset($input['brigada']);
        unset($input['lugar_destino_cat']);    
        
        // Este segmento es para sobreescribir las fechas que vienen entrando (Les quitamos 3 horas.)
        $input['fecha_despacho']    = $this->modificarFecha($input['fecha_despacho']);
        $input['fecha_salida']      = $this->modificarFecha($input['fecha_salida']);
        $input['fecha_arribo']      = $this->modificarFecha($input['fecha_arribo']);
        $input['fecha_inicio']      = $this->modificarFecha($input['fecha_inicio']);
        $input['fecha_termino']     = $this->modificarFecha($input['fecha_termino']);
        $input['fecha_regreso']     = $this->modificarFecha($input['fecha_regreso']);
        $input['fecha_arribo_base'] = $this->modificarFecha($input['fecha_arribo_base']);        

        $this->db->set($input);
        $this->db->update('siniestro', $input, array('id_siniestro'=>$input['id_siniestro']));

        $error = $this->db->error();
        if( !empty($error['message']) ){
            $estado = 'ERROR';
            $mensaje = 'Ha ocurrido un problema - Error N° '.$error['code'];
        }

        // Borramos la relación de Colaboradores con el Siniestro
        $sql = "DELETE FROM colaborador_siniestro WHERE id_siniestro = " . $input['id_siniestro'];
        $data =  $this->db->query($sql);
        $error = $this->db->error();
        
        if( !empty($error['message']) ){
            $estado = 'ERROR';
            $mensaje = 'Ha ocurrido un problema - Error N° '.$error['code'];
            $objResponse['estado'] = $estado;
            $objResponse['mensaje'] = $mensaje;
            $objResponse['objeto'] = null;    
            $this->response([$objResponse], REST_Controller::HTTP_OK);            
        }

        // Insert de Colaborador-Siniestro
        foreach( $colaboradores as $i ){
            $sqlInsert = "INSERT INTO colaborador_siniestro (id_colaborador, id_siniestro) VALUES (". $i['id_colaborador'] .", ". $input['id_siniestro'] .")";
            $data =  $this->db->query($sqlInsert);
            $error = $this->db->error();
            if( !empty($error['message']) ){
                $estado = 'ERROR';
                $mensaje = 'Ha ocurrido un problema - Error N° '.$error['code'];
                $objResponse['estado'] = $estado;
                $objResponse['mensaje'] = $mensaje;
                $objResponse['objeto'] = null;    
                $this->response([$objResponse], REST_Controller::HTTP_OK);            
            }            
        }

        $objResponse['estado'] = $estado;
        $objResponse['mensaje'] = $mensaje;
        $objResponse['objeto'] = $this->index_get( $input['id_siniestro'], false );

        $this->response([$objResponse], REST_Controller::HTTP_OK);       
    }

    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function actualizarSiniestro_put(){
        $estado = 'OK';
        $mensaje = 'Proceso Exitoso.';
        
        $input = $this->put();
        unset($input['brigada']);
        unset($input['lugar_destino_cat']);        

        // Este segmento es para sobreescribir las fechas que vienen entrando (Les quitamos 3 horas.)
        $input['fecha_despacho']    = $this->modificarFecha($input['fecha_despacho']);
        $input['fecha_salida']      = $this->modificarFecha($input['fecha_salida']);
        $input['fecha_arribo']      = $this->modificarFecha($input['fecha_arribo']);
        $input['fecha_inicio']      = $this->modificarFecha($input['fecha_inicio']);
        $input['fecha_termino']     = $this->modificarFecha($input['fecha_termino']);
        $input['fecha_regreso']     = $this->modificarFecha($input['fecha_regreso']);
        $input['fecha_arribo_base'] = $this->modificarFecha($input['fecha_arribo_base']);

        $this->db->set($input);
        $this->db->update('siniestro',$input, array('id_siniestro'=>$input['id_siniestro']));

        $error = $this->db->error();
        if( !empty($error['message']) ){
            $estado = 'ERROR';
            $mensaje = 'Ha ocurrido un problema - Error N° '.$error['code'];
        }

        $objResponse['estado'] = $estado;
        $objResponse['mensaje'] = $mensaje;
        $objResponse['objeto'] = $input;
        $this->response([$objResponse], REST_Controller::HTTP_OK);
    }

    public function modificarFecha($fecha){
        if ($fecha == NULL) {
            return null;
        }
        else{
            $fecha = str_replace('Z', '', $fecha);
            $fecha = substr($fecha, 0, -4);
            $fecha = $fecha. "-03:00";
            
            $dt = DateTime::createFromFormat(DateTime::ISO8601, $fecha);
            $dt->sub(new DateInterval('PT3H'));
            return $dt->format('Y-m-d H:i:s');
        }
    }
}