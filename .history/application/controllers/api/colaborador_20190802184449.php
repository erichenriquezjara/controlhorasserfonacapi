<?php
   
require APPPATH . 'controllers\REST_Controller.php';
     
class Colaborador extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = -1)
	{
        $sql = "SELECT * FROM colaborador WHERE IFNULL(cancelacion, 'N') != 'S'";

        if( $id != -1 ){
            $sql .= " AND id_colaborador = " . $id;
        }

        $data =  $this->db->query($sql)->result();
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $mensaje = 'Colaborador creado correctamente.';
        $input = $this->post();
        try{
            $this->db->set($input);
            $this->db->insert('colaborador',$input);
            $error = $this->db->error();
            var_dump($error);
        }
        catch(Exception $e){
            $mensaje = 'Error al crear el Mensaje';
        }

        $this->response([$mensaje], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put()
    {
        $input = $this->put();
        $this->db->set($input);
        $this->db->update('colaborador', $input, array('id_colaborador'=>$input['id_colaborador']));
        $this->response(['Proceso realizado correctamente.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {

    }    	
}