<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'controllers/api/Utils.php';
// require_once APPPATH . 'libraries/tfpdf/tfpdf.php';
require_once APPPATH . 'libraries/tfpdf/fpdf-multicell-table.php';
require_once APPPATH . 'libraries/PHPExcel.php';


define("_SYSTEM_TTFONTS", APPPATH . "libraries\\tfpdf\\font\\unifont\\");

class Informes extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		date_default_timezone_set('America/Santiago');
	}

    public function index()
	{
		$BasicPDF = new BasicPDF();
		// Títulos de las columnas
		$header = array('País', 'Capital', 'Superficie (km2)', 'Pobl. (en miles)');
		// Carga de datos
		$data = $BasicPDF->LoadData( APPPATH . 'controllers\reportes\paises.txt');
		$BasicPDF->SetFont('Arial','',14);
		$BasicPDF->AddPage();
		$BasicPDF->BasicTable($header,$data);
		$BasicPDF->AddPage();
		$BasicPDF->ImprovedTable($header,$data);
		$BasicPDF->AddPage();
		$BasicPDF->FancyTable($header,$data);
		$BasicPDF->Output();
	}

	public function obtenerAsistenciaBrigadaMes($id_brigada, $mes, $año){
		$sql = "CALL GENERA_TABLA_ASISTENCIA(".$id_brigada.",".$mes.",".$año.")";
		$resultados =  $this->db->query($sql)->result();
		$this->db->reconnect();
		$sql2 = "SELECT nombre_brigada FROM brigada WHERE IFNULL(cancelacion, 'N') != 'S' AND id_brigada = " . $id_brigada;
		$datosBrigada = $this->db->query($sql2)->first_row();		

		// Crear nuevo objeto PHPExcel
		$objPHPExcel = new PHPExcel();		
		
		// Propiedades del documento
		$objPHPExcel->getProperties()->setCreator("Eric Henríquez")
									->setLastModifiedBy("Eric Henríquez")
									->setTitle("Office 2010 XLSX - 3ngineers")
									->setSubject("Office 2010 XLSX - 3ngineers")
									->setDescription("Documento para Office 2010 XLSX, generado usando PHPExcel")
									->setKeywords("Office 2010 OpenXML PHP")
									->setCategory("Informes Serfonac");		

		// Fuente de la primera fila en negrita
		$boldArray = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));									
		$objPHPExcel->getActiveSheet()->getStyle()->applyFromArray($boldArray);

		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 2, 1, 'TARJA MENSUAL DE BRIGADA' );
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 2, 3, 'UNIDAD' );
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 3, 3, $datosBrigada->nombre_brigada );

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 2, 4, 'MES' );
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 3, 4, $mes );
		// var_dump($resultados);

		//Ancho de las columnas
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);		
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(45);

		$styleArray = 
			array(
				'font' => array(
					'name' => 'Arial',
					'size' => 10
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				),
				'borders' => array(
					'allborders'=> array(
						'style'=> PHPExcel_Style_Border::BORDER_THICK,
						'color'=> array(
							'argb' => '000'
						)
					)
				)				
			);

		$letrasMes;
		$diasMes;

		switch($mes){
			case 1:
				$diasMes = 31;
			break;
			case 2:
				$diasMes = 28;
			break;
			case 3:
				$diasMes = 31;
			break;
			case 4:
				$diasMes = 30;
			break;
			case 5:
				$diasMes = 31;
			break;
			case 6:
				$diasMes = 30;
			break;
			case 7:
				$diasMes = 31;
			break;
			case 8:
				$diasMes = 31;
			break;
			case 9:
				$diasMes = 30;
			break;
			case 10:
				$diasMes = 31;
			break;
			case 11:
				$diasMes = 30;
			break;
			case 12:
				$diasMes = 31;
			break;
		}

		switch($diasMes){
			case 28:
				$letrasMes = "AH";
			break;
			case 30:
				$letrasMes = "AJ";
			break;
			case 31:
				$letrasMes = "AK";
			break;
		}		
		
		// Este Ciclo itera sobre todas las fechas		
		$row = 9;
		$col = 4;

		// Para asignarle el estilo a la hoja
		$objPHPExcel->getActiveSheet()->getStyle("B8:".$letrasMes."".( $row + count($resultados) - 1 ) )->applyFromArray($styleArray);

		$ultimaFila;
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 1, $row-1, "N°" );
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 2, $row-1, "NOMBRE / APELLIDO" );
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 3, $row-1, "RUT" );
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 4, $row-1, "CARGO" );
		foreach ($resultados as $key => $value) {
			$array = (array) $value;
			$diasTrabajados = 30;
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 1, ($key + $row), ($key + 1) );
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 2, ($key + $row), $array['NOMBRES'] );
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 3, ($key + $row), $array['RUT'] );
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( $col, ($key + $row), $array['CARGO'] );
			for( $i=1; $i<32; $i++){
				if( $key == 0 ){
					if( isset( $array['dia_' . $i] ) ){
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( $col + $i + 1, ($key + $row - 1), "TOTAL" );
						//$objPHPExcel->getActiveSheet()->getCellByColumnAndRow( $col + $i + 1, ($key + $row - 1))->getStyle()->applyFromArray($styleArray);
						$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( $col + $i, ($key + $row - 1), $i );
						//$objPHPExcel->getActiveSheet()->getCellByColumnAndRow( $col + $i, ($key + $row - 1))->getStyle()->applyFromArray($styleArray);
					}
				}

				if( isset( $array['dia_' . $i] ) ){
					$ultimaColumna = $i;
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( $col + $i, ($key + $row), $array['dia_' . $i] );
					//$objPHPExcel->getActiveSheet()->getCellByColumnAndRow( $col + $i, ($key + $row))->getStyle()->applyFromArray($styleArray);

					if( $array['dia_' . $i] == "F" || $array['dia_' . $i] == "PE"){
						$diasTrabajados--;
					}
				}
			}

			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( $col + $ultimaColumna + 1, ($key + $row), $diasTrabajados );

			$ultimaFila = $key;
		}
		

		
		//exit();
		// Redirigir la salida al navegador web de un cliente ( Excel5 )
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Tarja Mensual de Brigada.xls"');
		header('Cache-Control: max-age=0');
		// Si usted está sirviendo a IE 9 , a continuación, puede ser necesaria la siguiente
		header('Cache-Control: max-age=1');

		// Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

        // Save Excel file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');		
	}

	public function horasExtrasPorBrigadaExcel($id = -1, $brigada = -1, $fechaDesde = -1, $fechaHasta = -1, $httpResponseFlag = true){
		$utils = new Utils();
		$data = $utils->siniestrosPorBrigada_get($id, $brigada, $fechaDesde, $fechaHasta, false);
		$datosBrigada = $utils->brigadaPorId_get($brigada, false)[0];

		if ( $fechaDesde != -1 && $fechaHasta != -1) {
			$rangoFechasTexto = $fechaDesde . ' - ' . $fechaHasta;
		}
		else{
			$rangoFechasTexto = 'No estipulado';
		}

		//var_dump($datosBrigada);
		//exit();

		/*
		var_dump($data);
		exit();		
		*/

		// Crear nuevo objeto PHPExcel
		$objPHPExcel = new PHPExcel();		
		
		// Propiedades del documento
		$objPHPExcel->getProperties()->setCreator("Eric Henríquez")
									->setLastModifiedBy("Eric Henríquez")
									->setTitle("Office 2010 XLSX - 3ngineers")
									->setSubject("Office 2010 XLSX - 3ngineers")
									->setDescription("Documento para Office 2010 XLSX, generado usando PHPExcel")
									->setKeywords("Office 2010 OpenXML PHP")
									->setCategory("Informes Serfonac");		

		// Fuente de la primera fila en negrita
		$boldArray = array('font' => array('bold' => true,),'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));									
		$objPHPExcel->getActiveSheet()->getStyle()->applyFromArray($boldArray);

		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 2, 1, 'HORAS EXTRA POR BRIGADA' );
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 2, 3, 'UNIDAD' );
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 3, 3, $datosBrigada->nombre_brigada );

		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 2, 4, 'FECHAS' );
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 3, 4, $rangoFechasTexto );


		$styleArray = 
			array(
				'font' => array(
					'name' => 'Arial',
					'size' => 10
				),
				'alignment' => array(
					'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				),
				'borders' => array(
					'allborders'=> array(
						'style'=> PHPExcel_Style_Border::BORDER_THICK,
						'color'=> array(
							'argb' => '000'
						)
					)
				)				
			);
		
		$header = array(
			"id_siniestro" => "ID Actividad",
			"nro_foco" => "N° Foco",
			"fuerza_combate_real" => "F. Combate",
			"codigo_actividad" => "Código A.",
			"fecha_despacho" => "Fecha Despacho",
			"fecha_termino" => "Termino R - 24",
			"fecha_arribo_base" => "Arribo a Base",
			"horas_sr" => "Horas SR",
			"t_horas_sr" => "T Horas SR",
			"horas_cr" => "Horas CR",
			"t_horas_cr" => "T Horas CR",
			"lugar_destino_texto" => "Lugar",
		);

		$valores = array(
			0 => "id_siniestro",
			1 => "nro_foco",
			2 => "fuerza_combate_real",
			3 => "codigo_actividad",
			4 => "fecha_despacho",
			5 => "fecha_termino",
			6 => "fecha_arribo_base",
			7 => "horas_sr",
			8 => "t_horas_sr",
			9 => "horas_cr",
			10 => "t_horas_cr",
			11 => "lugar_destino_texto"			
		);
		
		//var_dump($valores);
		//exit();
		
		// Este Ciclo itera sobre todas las fechas		
		$row = 9;
		$col = 4;

		error_reporting(0);
		foreach($valores as $key => $value){
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( $key + 1, $row-1, $header[$value] );
		}
		
		$ultimaFila;
		foreach ($data as $key => $value) {
			$array = (array) $value;
			foreach($valores as $key2 => $value2){
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( $key2 + 1, ($key + $row), $array[$valores[$key2]] );
			}

			$ultimaFila = $key;
		}

		$ultimaFila += $row;

		//echo $ultimaFila;
		//exit();

		// Para asignarle el estilo a la hoja
		$objPHPExcel->getActiveSheet()->getStyle("B8:M".$ultimaFila )->applyFromArray($styleArray);		

		// Redirigir la salida al navegador web de un cliente ( Excel5 )
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Horas Extra por Brigada.xls"');
		header('Cache-Control: max-age=0');
		// Si usted está sirviendo a IE 9 , a continuación, puede ser necesaria la siguiente
		header('Cache-Control: max-age=1');

		// Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

        // Save Excel file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');		
	}	

	public function actividades1($id = -1, $brigada = -1, $fechaDesde = -1, $fechaHasta = -1, $httpResponseFlag = true){
		$utils = new Utils();
		$data = $utils->siniestrosPorBrigada_get($id, $brigada, $fechaDesde, $fechaHasta, false);
		$brigada = $utils->brigadaPorId_get($brigada, false);
		$rangoFechas = "";

		if( $fechaDesde != -1 && $fechaHasta != -1){
			$fechaDesde = base64_decode($fechaDesde);
			$fechaHasta = base64_decode($fechaHasta);

			$fechaDesde = substr_replace($fechaDesde ,"", -9);
			$fechaHasta = substr_replace($fechaHasta ,"", -9);
			$rangoFechas = "(Rango: ". $fechaDesde . " - " . $fechaHasta .")";
		}
		else{
			$rangoFechas = "";
		}

		$header = array(
			//"id_siniestro" => "ID Actividad",
			"nro_foco" => "N° Foco                     ",
			"codigo_actividad" => "Código A.                     ",
			"centro_costo" => "C. Costo                     ",
			"lugar_destino_cat" => "Lugar                     ",
			"fuerza_combate" => "F. Combate                     ",
			"fecha_despacho" => "Fecha Despacho                     ",
			"fecha_salida" => "Salida o comienzo actividad (R29)",
			"fecha_arribo" => "Arribo R - 40                     ",
			"fecha_inicio" => "Inicio R - R24                     ",
			"fecha_termino" => "Termino R - 24                     ",
			"fecha_regreso" => "Regreso                     ",
			"fecha_arribo_base" => "Arribo a base o fin actividad",
			//"fecha_creacion" => "Fecha Creación ",
			//"id_brigada" => "Id Brigada"
		);

		$anchoColumnas = array(
			//"id_siniestro" => 10,
			"nro_foco" => 15,
			"codigo_actividad" => 15,
			"centro_costo" => 20,
			"lugar_destino_cat" => 15,
			"fuerza_combate" => 20,
			"fecha_despacho" => 28,
			"fecha_salida" => 28,
			"fecha_arribo" => 28,
			"fecha_inicio" => 28,
			"fecha_termino" => 28,
			"fecha_regreso" => 28,
			"fecha_arribo_base" => 28,
			//"fecha_creacion" => 20,
			//"id_brigada" => 20
		);

		$anchoColumnasN = array(
			// $anchoColumnas['id_siniestro'],
			$anchoColumnas['nro_foco'],
			$anchoColumnas['codigo_actividad'],
			$anchoColumnas['centro_costo'],
			$anchoColumnas['lugar_destino_cat'],
			$anchoColumnas['fuerza_combate'],
			$anchoColumnas['fecha_despacho'],
			$anchoColumnas['fecha_salida'],
			$anchoColumnas['fecha_arribo'],
			$anchoColumnas['fecha_inicio'],
			$anchoColumnas['fecha_termino'],
			$anchoColumnas['fecha_regreso'],
			$anchoColumnas['fecha_arribo_base']
			// $anchoColumnas['fecha_creacion'],
			// $anchoColumnas['id_brigada']
		);

		$alineacionColumnas = array(
			// $anchoColumnas['id_siniestro'],
			'C',
			'C',
			'C',
			'C',
			'C',
			'C',
			'C',
			'C',
			'C',
			'C',
			'C',
			'C'
			// $anchoColumnas['fecha_creacion'],
			// $anchoColumnas['id_brigada']
		);			

		// Este arreglo es para poder iterar sobre las columnas descritas en el $header.
		$headerArray = array();
		foreach ($header as $key => $value) {
			array_push($headerArray, $key);
		}		

		// Creamos el PDF
		$pdf = new PDF_MC_Table('L','mm','A4');
		$pdf->AddPage();

		// Insert a logo in the top-left corner at 300 dpi
		$pdf->Image( APPPATH . 'assets/serfonac.png',10,10,-500);
		

		// Título del archivo
		$tituloDocumento = 'BITACORA DE RECURSOS TERRESTRES (' . strtoupper($brigada[0]->nombre_brigada) . ") " . $rangoFechas;

		$pdf->SetTitle($tituloDocumento);
		
		// Título del documento
		$pdf->SetFont('Arial','',12);
		$pdf->SetFillColor(200,220,255);	
		$pdf->Cell(50);	
		$pdf->Cell(0,6, $tituloDocumento ,0,1,'L',true);
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();

        // Colores, ancho de línea.
        $pdf->SetFillColor(255,0,0);
        $pdf->SetTextColor(255);
        $pdf->SetDrawColor(0,0,0);
		$pdf->SetLineWidth(.5);
		

		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->SetFont('DejaVu','',8);		
			
        $pdf->Ln();
        // Restauración de colores y fuentes
        $pdf->SetFillColor(224,235,255);
		$pdf->SetTextColor(0);
		$pdf->SetAligns($alineacionColumnas);
		
        // Datos
		$fill = false;

		// Arreglo para Cargar la fila completa.
		$rowArray = Array();

		$pdf->SetWidths( $anchoColumnasN );

		// Warning que no entiendo pero la caga.
		error_reporting(0);
		

		// Agregamos la primera fila que corresponde a los headers al arreglo con datos.
		array_unshift($data, $header);

        foreach($data as $Rkey => $row)
        {
			foreach ($headerArray as $indice => $Hvalue) 
			{
				foreach ($row as $Ckey => $celda) {

					if ( $Ckey == $Hvalue ) {

						// Si es la primera fila.
						if ( $Rkey == 0) {
							array_push($rowArray, $celda);
						}
						else{
							if( $this->startsWith($Ckey, 'fecha_') ){
								if ( $celda == NULL ) {
									$celda = "";
								}
								else{
									$date = date_create( $celda );
									$celda = date_format( $date, 'd/m/y H:i');
								}
							}					
							
							if ( $Ckey == 'lugar_destino_cat' && $celda != null ){
								$celda = $celda->valor1;
							}
	
							if ( $Ckey == 'fuerza_combate' && $celda != null) {
								//var_dump($row);
								$celda = $row->brigada->tipo_brigada->valor1;
							}
		
							/*
							$pdf->Cell( $anchoColumnas[$Ckey], 14, $celda, 1, 0, 'C', true);
							*/
							array_push($rowArray, $celda);	
						}					
					}
				}
			}

			// Hay que verificar si se está insertando la primera u otra.
			if ( $Rkey == 0) {
				$pdf->Row( $rowArray, true );
			}
			else{
				$pdf->Row( $rowArray );
			}
			$rowArray = Array();
		}
		$pdf->Output();
	}

	public function horasExtraPorBrigada($brigada = -1, $fechaDesde = -1, $fechaHasta = -1){
        if( $fechaDesde != -1 && $fechaHasta != -1){
            $fechaDesde = base64_decode($fechaDesde);
            $fechaHasta = base64_decode($fechaHasta);
		}
		
		$utils = new Utils();
		$resultados = $utils->horasExtraBrigada($brigada, $fechaDesde, $fechaHasta);
		//var_dump($resultados);

		$dFechaDesde = $this->crearFecha($fechaDesde);
		$dfechaHasta = $this->crearFecha($fechaHasta);

		// echo $dFechaDesde->format('Y-m-d');

		// Crear nuevo objeto PHPExcel
		$objPHPExcel = new PHPExcel();

		// Propiedades del documento
		$objPHPExcel->getProperties()->setCreator("Eric Henríquez")
									->setLastModifiedBy("Eric Henríquez")
									->setTitle("Office 2010 XLSX - 3ngineers")
									->setSubject("Office 2010 XLSX - 3ngineers")
									->setDescription("Documento para Office 2010 XLSX, generado usando PHPExcel")
									->setKeywords("Office 2010 OpenXML PHP")
									->setCategory("Informes Serfonac");		

		$objPHPExcel->setActiveSheetIndex(0);

		// Este Ciclo itera sobre todas las fechas		
		$col = 1;
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 1, 1, 'Colaborador' );

		$arrayTotales = [];
		foreach ($resultados as $key => $value) {
			$arrayTotales[$key] = 0;
		}
		

		while( $dFechaDesde <= $dfechaHasta ) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( ($col + 1), 1, $dFechaDesde->format('d-m-Y') );
			foreach ($resultados as $key => $value) {
				$array = (array) $value;
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( 1, ($key + 2), $array['nombres'] );

				if ( isset( $array[ $dFechaDesde->format('Y-m-d')] ) ){
					$arrayTotales[$key] = $arrayTotales[$key] + $array[ $dFechaDesde->format('Y-m-d')];
					if ( $array[ $dFechaDesde->format('Y-m-d')] == 0) {
						$array[ $dFechaDesde->format('Y-m-d')] = '';
					}
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( ($col + 1), ($key + 2), $array[ $dFechaDesde->format('Y-m-d')] );
				}
				else{
					$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( ($col + 1), ($key + 2), '' );
				}
			}	
			$col++;	
			$dFechaDesde = $this->sumarUnDia($dFechaDesde);		
		}

		// Última columna con totales
		$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( ($col + 1), 1, "TOTAL" );
		foreach ($resultados as $key => $value) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow( ($col + 1), ($key + 2), $arrayTotales[$key] );
		}


		// Redirigir la salida al navegador web de un cliente ( Excel5 )
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Horas Extra Por Brigada.xls"');
		header('Cache-Control: max-age=0');
		// Si usted está sirviendo a IE 9 , a continuación, puede ser necesaria la siguiente
		header('Cache-Control: max-age=1');

		// Si usted está sirviendo a IE a través de SSL , a continuación, puede ser necesaria la siguiente
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

        // Save Excel file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
	}
	
	public function horasExtra1($id = -1, $brigada = -1, $fechaDesde = -1, $fechaHasta = -1, $httpResponseFlag = true){
		$utils = new Utils();
		$data = $utils->siniestrosPorBrigada_get($id, $brigada, $fechaDesde, $fechaHasta, false);
		$brigada = $utils->brigadaPorId_get($brigada, false);
		$rangoFechas = "";

		if( $fechaDesde != -1 && $fechaHasta != -1){
			$fechaDesde = base64_decode($fechaDesde);
			$fechaHasta = base64_decode($fechaHasta);

			$fechaDesde = substr_replace($fechaDesde ,"", -9);
			$fechaHasta = substr_replace($fechaHasta ,"", -9);
			$rangoFechas = "(Rango: ". $fechaDesde . " - " . $fechaHasta .")";
		}
		else{
			$rangoFechas = "";
		}
		
		/*
		echo "<pre>";
		var_dump($data);
		echo "</pre>";
		exit();
		*/
		

		$header = array(
			"id_siniestro" => "ID Actividad                     ",
			"nro_foco" => "N° Foco                     ",
			"fuerza_combate_real" => "F. Combate                     ",
			"codigo_actividad" => "Código A.                     ",
			"fecha_despacho" => "Fecha Despacho                     ",
			"fecha_termino" => "Termino R - 24                     ",
			"fecha_arribo_base" => "Arribo a Base                     ",
			"horas_sr" => "Horas SR                    ",
			"t_horas_sr" => "T Horas SR                    ",
			"horas_cr" => "Horas CR                    ",
			"t_horas_cr" => "T Horas CR                    ",			
			"lugar_destino" => "Lugar                     ",
			//"observacion" => "Observación                     "
		);

		$anchoColumnas = array(
			"id_siniestro" => 25,
			"nro_foco" => 25,
			"fuerza_combate_real" => 25,
			"codigo_actividad" => 25,
			"fecha_despacho" => 25,
			"fecha_termino" => 25,
			"fecha_arribo_base" => 25,
			"horas_sr" => 20,
			"t_horas_sr" => 20,
			"horas_cr" => 20,
			"t_horas_cr" => 20,			
			"lugar_destino" => 20,
			//"observacion" => 25
		);

		$anchoColumnasN = array(
			$anchoColumnas['id_siniestro'],
			$anchoColumnas['nro_foco'],
			$anchoColumnas['fuerza_combate_real'],
			$anchoColumnas['codigo_actividad'],
			$anchoColumnas['fecha_despacho'],
			$anchoColumnas['fecha_termino'],
			$anchoColumnas['fecha_arribo_base'],
			$anchoColumnas['horas_sr'],
			$anchoColumnas['t_horas_sr'],			
			$anchoColumnas['horas_cr'],
			$anchoColumnas['t_horas_cr'],
			$anchoColumnas['lugar_destino'],
			//$anchoColumnas['observacion']
		);

		$alineacionColumnas = array(
			'C',
			'C',
			'C',
			'C',
			'C',
			'C',
			'C',
			'C',
			'C',
			'C',
			'C',
			'C'			
			//'C'
		);			

		// Este arreglo es para poder iterar sobre las columnas descritas en el $header.
		$headerArray = array();
		foreach ($header as $key => $value) {
			array_push($headerArray, $key);
		}		

		// Creamos el PDF
		$pdf = new PDF_MC_Table('L','mm','A4');
		$pdf->AddPage();

		/*
		var_dump($brigada);
		exit();
		*/

		// Insert a logo in the top-left corner at 300 dpi
		$pdf->Image( APPPATH . 'assets/serfonac.png',10,10,-500);
		

		// Título del archivo
		$tituloDocumento = 'HORAS EXTRA POR BRIGADA (' . strtoupper($brigada[0]->nombre_brigada) . ") " . $rangoFechas;

		$pdf->SetTitle($tituloDocumento);
		
		// Título del documento
		$pdf->SetFont('Arial','',12);
		$pdf->SetFillColor(200,220,255);	
		$pdf->Cell(50);	
		$pdf->Cell(0,6, $tituloDocumento ,0,1,'L',true);
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();

        // Colores, ancho de línea.
        $pdf->SetFillColor(255,0,0);
        $pdf->SetTextColor(255);
        $pdf->SetDrawColor(0,0,0);
		$pdf->SetLineWidth(.5);
		

		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->SetFont('DejaVu','',8);		
		
		// Cabecera
		/*
        foreach ($header as $key => $value) {
			$pdf->Cell( $anchoColumnas[$key], 7, $value, 1, 0, 'C', true);
		}
		*/
			
        $pdf->Ln();
        // Restauración de colores y fuentes
        $pdf->SetFillColor(224,235,255);
		$pdf->SetTextColor(0);
		$pdf->SetAligns($alineacionColumnas);
		
        // Datos
		$fill = false;

		// Arreglo para Cargar la fila completa.
		$rowArray = Array();

		$pdf->SetWidths( $anchoColumnasN );

		// Warning que no entiendo pero la caga.
		error_reporting(0);
		

		// Agregamos la primera fila que corresponde a los headers al arreglo con datos.
		array_unshift($data, $header);

        foreach($data as $Rkey => $row)
        {
			foreach ($headerArray as $indice => $Hvalue) 
			{
				foreach ($row as $Ckey => $celda) {

					if ( $Ckey == $Hvalue ) {

						// Si es la primera fila.
						if ( $Rkey == 0) {
							array_push($rowArray, $celda);
						}
						else{
							if( $this->startsWith($Ckey, 'fecha_') ){
								if ( $celda == NULL ) {
									$celda = "";
								}
								else{
									$date = date_create( $celda );
									$celda = date_format( $date, 'd/m/y H:i');
								}
							}					
							
							if ( $Ckey == 'lugar_destino_cat' && $celda != null ){
								$celda = $celda->valor1;
							}
	
							if ( $Ckey == 'fuerza_combate' && $celda != null) {
								//var_dump($row);
								$celda = $row->brigada->tipo_brigada->valor1;
							}

							if ( $Ckey == 'horas' && $celda != null ) {
								$celda = number_format($celda, 2, '.', '');
							}

							if ( $Ckey == 't_horas' && $celda != null ) {
								$celda = number_format($celda, 2, '.', '');
							}							
		
							/*
							$pdf->Cell( $anchoColumnas[$Ckey], 14, $celda, 1, 0, 'C', true);
							*/
							array_push($rowArray, $celda);	
						}					
					}
				}
			}

			// Hay que verificar si se está insertando la primera u otra.
			if ( $Rkey == 0) {
				$pdf->Row( $rowArray, true );
			}
			else{
				$pdf->Row( $rowArray );
			}
			$rowArray = Array();
		}
		$pdf->Output();
	}

	// Auxiliar pa cachar si un string parte con cierto string
	function startsWith ($string, $startString) 
	{ 
		$len = strlen($startString); 
		return (substr($string, 0, $len) === $startString); 
	} 

	public function sumarUnDia($dt){
        $dt->add(new DateInterval('P1D'));
        return $dt;
    }

	public function crearFecha($fechaStr){
		// Crea una fecha a partir de un string con formato Y-m-d
		$formato = 'Y-m-d';
		$fecha = DateTime::createFromFormat($formato, $fechaStr);
        return $fecha;
	}
}

?>