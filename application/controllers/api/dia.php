<?php
   
require APPPATH . 'controllers/REST_Controller.php';
     
class Dia extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->db->get_where("dia", ['id_dia' => $id])->row_array();
        }
        else{
            $data = $this->db->get("dia")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	} 

    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function obtenerAsistenciaBrigadaMes_get($id_brigada, $mes, $año, $httpResponseFlag = true)
    {
        $sql = "CALL GENERA_TABLA_ASISTENCIA(".$id_brigada.",".$mes.",".$año.")";
        $data =  $this->db->query($sql)->result();       
        if (  $httpResponseFlag == true ){
            $this->response($data, REST_Controller::HTTP_OK);
        }
        else{
            return $data;
        }
    }

    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $estado = 'OK';
        $mensaje = 'Proceso Exitoso.';
        $input = $this->post();

        $sqlDelete = 'DELETE FROM dia 
        WHERE id_colaborador = '.$input['id_colaborador'].' 
        AND id_brigada = '.$input['id_brigada'].' 
        AND dia = '.$input['dia'].' 
        AND mes = '.$input['mes'].' 
        AND año = '.$input['año'];

        $this->db->query($sqlDelete);
        $this->db->insert('dia',$input);

        $error = $this->db->error();
        if( !empty($error['message']) ){
            $estado = 'ERROR';
            $mensaje = 'Ha ocurrido un problema - Error N° '.$error['code'];
        } 

        $sqlColor = 'SELECT OBTENER_COLOR_DIA_TRABAJADO('.$input['id_colaborador'].','.$input['id_brigada'].','.$input['dia'].','.$input['mes'].','.$input['año'].') AS color';
        $color = $this->db->query($sqlColor)->first_row();
        $input['color'] = $color->color;

        $response['estado'] = $estado;
        $response['mensaje'] =  $mensaje;
        $response['objeto'] = $input;

        $this->response($response, REST_Controller::HTTP_OK);
    }    
}