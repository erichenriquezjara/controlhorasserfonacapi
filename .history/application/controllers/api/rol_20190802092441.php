<?php
   
require APPPATH . 'controllers\REST_Controller.php';
     
class Rol extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->db->get_where("rol", ['id_rol' => $id])->row_array();
        }
        else{
            $data = $this->db->get("rol")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $input = $this->post();

        $this->db->set($input);
        $this->db->insert('rol',$input);
     
        $this->response(['rol created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put()
    {
        var_dump($this->put());
        $input = $this->put();
        $this->db->update('rol', $input, array('id_rol'=>$input['id_rol']));
     
        $this->response(['rol updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('rol', array('id_rol'=>$id));
        $this->db->
       
        $this->response(['rol deleted successfully.'], REST_Controller::HTTP_OK);
    }    	

    public function rolesColaborador_get($id = 0)
	{

        if(!empty($id)){
            $data = $this->db->get_where("rol", ['id_rol' => $id])->row_array();
        }
        else{
            $data = $this->db->get("rol")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
}