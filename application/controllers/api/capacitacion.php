<?php
   
require APPPATH . 'controllers/REST_Controller.php';
     
class Capacitacion extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->db->get_where("capacitacion", ['id_capacitacion' => $id])->row_array();
        }
        else{
            $data = $this->db->get("capacitacion")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $input = $this->post();

        $this->db->set($input);
        $this->db->insert('capacitacion',$input);
     
        $this->response(['capacitacion created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put()
    {
        var_dump($this->put());
        $input = $this->put();
        $this->db->update('capacitacion', $input, array('id_capacitacion'=>$input['id_capacitacion']));
     
        $this->response(['capacitacion updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('capacitacion', array('id_capacitacion'=>$id));
        $this->db->
       
        $this->response(['capacitacion deleted successfully.'], REST_Controller::HTTP_OK);
    }    	
}