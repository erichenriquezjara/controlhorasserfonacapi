<?php
   
require APPPATH . 'controllers\REST_Controller.php';
     
class Catalogo extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->db->get_where("catalogo", ['id_catalogo' => $id])->row_array();
        }
        else{
            $data = $this->db->get("catalogo")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $input = $this->post();

        $this->db->set($input);
        $this->db->insert('catalogo',$input);
     
        $this->response(['catalogo created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put()
    {
        var_dump($this->put());
        $input = $this->put();
        $this->db->update('catalogo', $input, array('id_catalogo'=>$input['id_catalogo']));
     
        $this->response(['catalogo updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('catalogo', array('id_catalogo'=>$id));
        $this->db->
       
        $this->response(['catalogo deleted successfully.'], REST_Controller::HTTP_OK);
    }    	
}