<?php
   
require APPPATH . 'controllers/REST_Controller.php';
     
class Menu extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function menusPorRol_get($id = -1, $httpResponseFlag = true)
	{
        $sql = "select m.* from menu m join menu_rol mr on mr.id_menu = m.id_menu where m.tipo = 'menu' and m.id_padre is null";

        if( $id != -1 ){
            $sql .= " AND mr.id_rol = " . $id;
        }

        $data =  $this->db->query($sql)->result();

        // Cargamos el tipo de brigada
        foreach( $data as $i ){
            $sqlSubmenu = "select * from menu m where m.tipo = 'submenu' and m.id_padre = ". $i->id_menu;
            $submenus = $this->db->query($sqlSubmenu)->result();
            if($submenus){
                $i->submenus = $this->db->query($sqlSubmenu)->result();
            }
        }

        $this->response($data, REST_Controller::HTTP_OK);
    }
    
}