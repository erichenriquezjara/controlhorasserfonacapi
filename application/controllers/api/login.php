<?php
   
require APPPATH . 'controllers/REST_Controller.php';
     
class Login extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();

       $this->load->helper(['jwt', 'authorization']);
    }

    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->db->get_where("colaborador", ['id_colaborador' => $id])->row_array();
        }
        else{
            $data = $this->db->get("colaborador")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $resultado = false;

        $usuario = $this->post('usuario');
        $contrasena = $this->post('contrasena');
        $sql = "select id_colaborador, nombres from colaborador where es_usuario = 1 and rut = '".$usuario."' and contrasena ='".$contrasena."'";
        $row =  $this->db->query($sql)->first_row();
        $token = AUTHORIZATION::generateToken($row);
        
        //Consulta para obtener los roles del usuario que se está logueando
        $sqlRoles = "select r.id_rol, r.nombre_rol from colaborador_rol cr join rol r on r.id_rol = cr.id_rol where cr.id_colaborador = ".$row->id_colaborador;
        $rolesUsuario = $this->db->query($sqlRoles)->result();

        //Consulta para obtener los menus del usuario
        
        foreach($rolesUsuario as $rol){
            $sqlMenu = "select m.* from menu m join menu_rol mr on mr.id_menu = m.id_menu where m.tipo = 'menu' and m.id_padre is null and mr.id_rol =".$rol->id_rol;
            $data =  $this->db->query($sqlMenu)->result();
            // Cargamos submenus
            foreach( $data as $i ){
                $sqlSubmenu = "select * from menu m where m.tipo = 'submenu' and m.id_padre = ".$i->id_menu;
                $submenus = $this->db->query($sqlSubmenu)->result();
                if($submenus){
                    $i->submenus = $this->db->query($sqlSubmenu)->result();
                }
            }
        }
        
        // Agrego los roles al objeto 
        $row->role = $rolesUsuario;

        //Agrego los menu al objeto
        $row->menus = $data;

        $this->response($row, REST_Controller::HTTP_OK);
    } 
	

     

}