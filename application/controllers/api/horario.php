<?php
   
require APPPATH . 'controllers/REST_Controller.php';
     
class Horario extends REST_Controller {
    
	/**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->db->get_where("horario", ['id_horario' => $id])->row_array();
        }
        else{
            $data = $this->db->get("horario")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $input = $this->post();

        $this->db->set($input);
        $this->db->insert('horario',$input);
     
        $this->response(['horario created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put()
    {
        var_dump($this->put());
        $input = $this->put();
        $this->db->update('horario', $input, array('id_horario'=>$input['id_horario']));
     
        $this->response(['horario updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('horario', array('id_horario'=>$id));
        $this->db->
       
        $this->response(['horario deleted successfully.'], REST_Controller::HTTP_OK);
    }    	
}