<?php
   
require APPPATH . 'controllers/REST_Controller.php';
     
class Registro_Horas extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        if(!empty($id)){
            $data = $this->db->get_where("registro_horas", ['id_registro_horas' => $id])->row_array();
        }
        else{
            $data = $this->db->get("registro_horas")->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
    }
    
    public function porBrigada_get($idBrigada = 0, $fDesde, $fHasta){
        $sql = "CALL OBTENER_HORAS_EXTRAS_BRIGADA(".$idBrigada.",'".$fDesde."','".$fHasta."')";
        // echo $sql;
        $data =  $this->db->query($sql)->result();
        $this->response($data, REST_Controller::HTTP_OK);
    }

    public function horasExtrasPorZona_get($idColaborador = 0, $fDesde, $fHasta){
        $sql = "CALL OBTENER_HORAS_EXTRAS_RESUMEN(".$idColaborador.",'".$fDesde."','".$fHasta."')";
        $data =  $this->db->query($sql)->result();
        $this->response($data, REST_Controller::HTTP_OK);
    }
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $input = $this->post();

        $sqlDelete = "DELETE FROM registro_horas WHERE id_siniestro = ". $input['id_siniestro'] ." AND id_brigada = ". $input['id_brigada'];

        // if ( isset($input['fecha_horas_extras']) ){
        //     $sqlDelete = $sqlDelete . " AND fecha_horas_extras = '" . $this->crearFecha( $input['fecha_horas_extras'] ) . "'";
        // }

        $this->db->query($sqlDelete);

        // Insert de los valores.
        $this->db->set($input);

        // var_dump($input);
        // echo $input['fecha_horas_extras'];
        $input['fecha_horas_extras'] = $this->modificarFecha( $input['fecha_horas_extras']);
        // echo $input['fecha_horas_extras'];
        $this->db->insert('registro_horas',$input);
        $this->response(['registro_horas created successfully.'], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put()
    {
        var_dump($this->put());
        $input = $this->put();
        $this->db->update('registro_horas', $input, array('id_registro_horas'=>$input['id_registro_horas']));
     
        $this->response(['registro_horas updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('registro_horas', array('id_registro_horas'=>$id));
        $this->db->
       
        $this->response(['registro_horas deleted successfully.'], REST_Controller::HTTP_OK);
    }    
    
    public function crearFecha($fecha){
        $fecha = str_replace('Z', '', $fecha);
        $fecha = substr($fecha, 0, -4);
        $fecha = $fecha. "-03:00";
        $dt = DateTime::createFromFormat(DateTime::ISO8601, $fecha);
        return $dt->format('Y-m-d');
    }
    
    public function modificarFecha($fecha){
        if ($fecha == NULL) {
            return null;
        }
        else{
            $fecha = str_replace('Z', '', $fecha);
            $fecha = substr($fecha, 0, -4);
            $fecha = $fecha. "-03:00";
            
            $dt = DateTime::createFromFormat(DateTime::ISO8601, $fecha);
            $dt->sub(new DateInterval('PT3H'));
            return $dt->format('Y-m-d H:i:s');
        }
    }
}