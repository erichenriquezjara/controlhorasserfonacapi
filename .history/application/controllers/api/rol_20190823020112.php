<?php
   
require APPPATH . 'controllers/REST_Controller.php';
     
class Rol extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = -1, $httpResponseFlag = true)
	{
        $sql = "SELECT * FROM rol WHERE IFNULL(cancelacion, 'N') != 'S'";

        if( $id != -1 ){
            $sql .= " AND id_rol = " . $id;
        }

        $data =  $this->db->query($sql)->result();

        if( $httpResponseFlag){
            $this->response($data, REST_Controller::HTTP_OK);
        }
        else{
            return $data;
        }
	}
      
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_post()
    {
        $estado = 'OK';
        $mensaje = 'Proceso Exitoso.';
        $input = $this->post();
        $this->db->set($input);
        
        $this->db->insert('rol',$input);
        $id_inserted = $this->db->insert_id();
      
        
        $error = $this->db->error();
        if( !empty($error['message']) ){
            $estado = 'ERROR';
            $mensaje = 'Ha ocurrido un problema - Error N° '.$error['code'];
        }

        $objResponse['estado'] = $estado;
        $objResponse['mensaje'] = $mensaje;
        $objResponse['objeto'] = $this->index_get($id_inserted, false );

        $this->response([$objResponse], REST_Controller::HTTP_OK);
    } 
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_put()
    {
        $estado = 'OK';
        $mensaje = 'Proceso Exitoso.';
        $input = $this->put();

        $this->db->set($input);
        $this->db->update('rol', $input, array('id_rol'=>$input['id_rol']));

        $error = $this->db->error();
        if( !empty($error['message']) ){
            $estado = 'ERROR';
            $mensaje = 'Ha ocurrido un problema - Error N° '.$error['code'];
        }

        $objResponse['estado'] = $estado;
        $objResponse['mensaje'] = $mensaje;
        $objResponse['objeto'] = $this->index_get( $input['id_rol'], false );

        $this->response([$objResponse], REST_Controller::HTTP_OK);
    }
     
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->db->delete('rol', array('id_rol'=>$id));
        $this->db->
       
        $this->response(['rol deleted successfully.'], REST_Controller::HTTP_OK);
    }    	

    public function rolesColaborador_get($id = 0)
	{
        $sql = "select r.* from colaborador cl join colaborador_rol cr on cr.id_colaborador = cl.id_colaborador join rol r on r.id_rol = cr.id_rol where cl.id_colaborador =".$id;
        if(!empty($id)){
            $data = $this->db->query($sql)->result();
        }
     
        $this->response($data, REST_Controller::HTTP_OK);
	}
}