<?php
   
require APPPATH . 'controllers/REST_Controller.php';
     
class Utils extends REST_Controller {
    
	/**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function obtenerAsistenciaBrigadaMes_get($id_brigada, $mes, $año, $httpResponseFlag = true)
    {
        $sql = "CALL GENERA_TABLA_ASISTENCIA(".$id_brigada.",".$mes.",".$año.")";
        $data =  $this->db->query($sql)->result();

        $data2 = $this->obtenerBrigada($id_brigada);

        if (  $httpResponseFlag == true ){
            $this->response($data, REST_Controller::HTTP_OK);
        }
        else{
            return $data;
        }
    }    
    
	public function siniestrosPorBrigada_get($id = -1, $brigada = -1, $fechaDesde = -1, $fechaHasta = -1, $httpResponseFlag = true)
	{
        $sql =  "SELECT *, 
                (SELECT valor FROM serfona1_controlhoras.catalogo where nombre_catalogo = 'LUGAR_DESTINO' and id_catalogo = s.lugar_destino LIMIT 1) lugar_destino_texto,
                (SELECT horas_extras_sr FROM registro_horas where id_siniestro = s.id_siniestro) AS horas_sr,
                (SELECT t_horas_extras_sr FROM registro_horas where id_siniestro = s.id_siniestro) AS t_horas_sr,
                (SELECT horas_extras_cr FROM registro_horas where id_siniestro = s.id_siniestro) AS horas_cr,
                (SELECT t_horas_extras_cr FROM registro_horas where id_siniestro = s.id_siniestro) AS t_horas_cr
                FROM siniestro s WHERE IFNULL(cancelacion, 'N') != 'S'";

        if( $id != -1 ){
            $sql .= " AND id_siniestro = " . $id;
        }

        if( $brigada != -1 ){
            $sql .= " AND id_brigada = " . $brigada;
        }

        if( $fechaDesde != -1 && $fechaHasta != -1){
            $fechaDesde = base64_decode($fechaDesde);
            $fechaHasta = base64_decode($fechaHasta);
            $sql .= " AND fecha_despacho between str_to_date('".$fechaDesde."', '%d/%m/%Y') AND str_to_date('".$fechaHasta."', '%d/%m/%Y')";
        }

        // Este true se cambia por false dependiendo de una futura variable o si se quieren contabilizar todas las actividades.
        if( false ) {            
            $sql .= " AND 1 = (SELECT valor2 FROM serfona1_controlhoras.catalogo WHERE nombre_catalogo = 'ACTIVIDADES' AND valor1 = codigo_actividad)";
            $sql .= " AND (SELECT SUM(t_horas_extras_cr) FROM registro_horas WHERE id_siniestro = s.id_siniestro) > 0";
        }        

        /*
        echo $sql;
        exit();
        */
        
        
        

        $sql .= " order by id_siniestro";
        $data =  $this->db->query($sql)->result();


        // Cargamos la brigada
        foreach( $data as $i ){
            $i->brigada = $this->brigadaPorId_get($i->id_brigada, false)[0];
            /*
            $sqlBrigada = "select * from brigada where id_brigada =".$i->id_brigada;
            $i->brigada = $this->db->query($sqlBrigada)->first_row();
            */
        }       

        // Cargamos el Lugar o Destino
        foreach( $data as $i ){
            $sqlLugarDestino = "SELECT id_catalogo, valor, valor1 FROM catalogo WHERE nombre_catalogo = 'LUGAR_DESTINO' AND id_catalogo = '".$i->lugar_destino."' AND IFNULL(cancelacion, 'N') != 'S'";
            $i->lugar_destino_cat = $this->db->query($sqlLugarDestino)->first_row();
        }        

        if( $httpResponseFlag ){
            $this->response($data, REST_Controller::HTTP_OK);
        }
        else{
            return $data;
        }    

    }

    public function brigadaPorId_get($id = -1, $httpResponseFlag = true)
	{
        $sql = "SELECT * FROM brigada WHERE IFNULL(cancelacion, 'N') != 'S'";

        if( $id != -1 ){
            $sql .= " AND id_brigada = " . $id;
        }

        $data =  $this->db->query($sql)->result();

        // Cargamos el tipo de brigada
        foreach( $data as $i ){
            $sqlTipoBrigada = "SELECT id_catalogo, clave, clave1, clave2, valor, valor1, valor2 FROM catalogo WHERE nombre_catalogo = 'TIPO BRIGADA' AND id_catalogo = '".$i->tipo_brigada."' AND IFNULL(cancelacion, 'N') != 'S'";
            $i->tipo_brigada = $this->db->query($sqlTipoBrigada)->first_row();
        }

        // Cargamos el Jefe de la Brigada
        foreach( $data as $i ){
            $sqlJefeBrigada = "SELECT col.* FROM colaborador col INNER JOIN catalogo ca ON col.cargo = ca.id_catalogo WHERE ca.valor = 'JEFE BRIGADA' AND col.id_colaborador = " . $i->jefe_brigada;
            $i->jefe_brigada = $this->db->query($sqlJefeBrigada)->first_row();
        }

        if( $httpResponseFlag){
            $this->response($data, REST_Controller::HTTP_OK);
        }
        else{
            return $data;
        }
    } 
    
    public function horasExtraBrigada($idBrigada = 0, $fDesde, $fHasta){
        $sql = "CALL OBTENER_HORAS_EXTRAS_BRIGADA(".$idBrigada.",'".$fDesde."','".$fHasta."')";
        $data =  $this->db->query($sql)->result();
        return $data;
    }
    
    public function obtenerBrigada($id = -1)
	{
        $sql = "SELECT * FROM brigada WHERE IFNULL(cancelacion, 'N') != 'S' AND id_brigada = " . $id;
        $data =  $this->db->query($sql)->result();
        return $data;
	}    
}